class Node{
    constructor(val){
        this.val=val
        this.next=null
    }
}
class LinkedList{
    constructor(){
        this.head=null
        this.tail=null
        this.length=0
    }
    push(val){
        var newNode=new Node(val)
        if(!this.head){
            this.head=newNode
            this.tail=newNode
        }
        this.tail.next=newNode
        this.tail=newNode
    }
    traverse(){
        var current=this.head
        while(current){
            console.log(current.val)
            current=current.next
        }
    }
}
obj=new LinkedList()
obj.push("HIG")
obj.push("Ogoo")
obj.push("56td")
obj.traverse()