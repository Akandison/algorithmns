function outerRecursive(arr){
    let outerval=[];
    function helper(newarr){
           if(newarr.length==0){
           return;
       }
        if(newarr[0]%2!=0){
            outerval.push(newarr[0]);
        }
      
    
        helper( newarr.slice(1));
    }
    helper(arr);
    return outerval;
}
console.log(outerRecursive([1,2,3,4,5,6,7,8]))