function getDigit(num,pos){
    return Math.floor(Math.abs(num)/Math.pow(10,pos))%10
    
}
//console.log(getDigit(987659856423,0))
function digiCount(num){
    if(num==0){return 1}
    newNum=Math.abs(num)
    return Math.floor(Math.log10 (newNum))+1
}
//console.log(digiCount(956736))
function mostDigits(arrNum){
    maxDigits=0
    for(let i=0;i<arrNum.length;i++){
        maxDigits=Math.max(maxDigits,digiCount(arrNum[i]))

    }
    return maxDigits
}
function radix(nums){
    let maxDigitCount=mostDigits(nums)
    for(let k=0;k<maxDigitCount;k++){
    let buckets=Array.from({length:10},()=>[])
    for(let i=0;i<nums.length;i++){
      let digits= getDigit(nums[i],k)
      buckets[digits].push(nums[i])
    }
   console.log(buckets)
    nums=[].concat(...buckets)
    console.log(nums)
  
    }
      return nums
}
radix([32,56,43,12,98,678,5,1])