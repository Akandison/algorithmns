class Node{
    constructor(val){
        this.val =val
        this.next=null
    }
}
class Queue{
    constructor(){
        this.first=null
        this.last=null
        this.size=0
    }
    enqueue(val){
        var newNode=new Node(val)
        if(this.size==0){
            this.first=newNode
            this.last=newNode
        }
        this.last.next=newNode
        this.last=newNode
        this.size++
        return this
    }
    dequeue(){
        while(this.size>0){
        if(!this.first)return null
        if(this.first==this.last) {this.last=null
        this.last=null}
        var oldFirst=this.first
        this.first=this.first.next
        oldFirst.next=null
        this.size--
        return oldFirst
        }
    }
    print(){
        var arr=[]
        var current=this.first
        while(current){
            arr.push(current.val)
            current=current.next
        }
        return arr
    }
}
var obj=new Queue()