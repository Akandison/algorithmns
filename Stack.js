class Node{
    constructor(val){
        this.val=val
        this.next=null

    }
}
class Stack{
    constructor(){
        this.size=0
        this.first=null
        this.last=null
    }
    push(val){
        var newNode=new Node(val)
        if(!this.first)
        {
            this.first=newNode
            this.last=newNode
        }
        var current=this.first
        newNode.next=current
        this.first=newNode
        this.size++
       return this
    }
    pop(){
        var oldFirst=this.first
        this.first.next=null
        this.first=oldFirst.next
        this.size--
        return oldFirst

        
    }
    print(){
        var current=this.first
        var arr=[]
        while(current){
            arr.push(current.val)
            current=current.next
        }
        
        return arr
    }
}
var obj=new Stack()
