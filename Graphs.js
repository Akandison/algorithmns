class Graph{
    constructor(){
     this.adjacencyList={}   
    }
    addVertix(nameOfVertix){
        if(!this.adjacencyList[nameOfVertix]) this.adjacencyList[nameOfVertix]=[]
        else console.log("Vertix already exists")
    }
    addEdge(vtx1,vtx2){
        if(this.adjacencyList[vtx1]) this.adjacencyList[vtx1].push(vtx2)
        if(this.adjacencyList[vtx2]) this.adjacencyList[vtx2].push(vtx1)
    }
    removeEdge(vtx1,vtx2){
       this.adjacencyList[vtx1]= this.adjacencyList[vtx1].filter(v=>v!=vtx2)
        this.adjacencyList[vtx2]=this.adjacencyList[vtx2].filter(v=>v!=vtx1)
    }
    removeVertex(vtx){
        while(this.adjacencyList[vtx].length){
            const adjacencyList=this.adjacencyList[vtx].pop()
            this.removeEdge(adjacencyList,vtx)
        }
        delete this.adjacencyList[vtx]
    }
}
var obj=new Graph()
obj.addVertix("Ghana")
obj.addVertix("Kumasi")
obj.addEdge("Ghana","Kumasi")
obj.removeEdge("Ghana","Kumasi")
obj.addVertix("Mim")
obj.addVertix("Gum")
obj.addEdge("Mim","Gum")
obj.addEdge("Ghana","Mim")
