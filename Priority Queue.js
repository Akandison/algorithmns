class PriorityQueue{
    constructor(){
        this.values=[]
    }
        enqueue(val,priority){
            var newNode=new Node(val,priority)
            this.values.push(newNode)
            this.bubble()
    }
    bubble(){
        let index=this.values.length-1
        const element=this.values[index]
        while(index>0){
        let parentIndex=Math.floor((index-1)/2)
        let parent=this.values[parentIndex]
        if(element.priority<=parent.priority)break
        this.values[parentIndex]=element
        this.values[index]=parent
        index=parentIndex
        } 
    }
    dequeue(){
        let max=this.values[0]
        let end=this.values.pop()
        if(this.values.length>0){
        this.values[0]=end
        this.sinkDown()
    }
        return max
    }
    sinkDown(){
       let index=0
       const length=this.values.length
       const element=this.values[0]

       while(true){
          let leftIndex=2*index+1
           let rightIndex=2*index+2
           let swap=null
           let rightChild,leftChild
           if(leftIndex<length){
                leftChild=this.values[leftIndex]
               if(element.priority<leftChild.priority){
                   swap=leftIndex
               }
           }
           if(rightIndex<length){
                rightChild=this.values[rightIndex]
               if(swap==null && element.priority<rightChild.priority || swap!=null && rightChild.priority>leftChild.priority){
                   swap=rightIndex
               }
           }
           if(swap==null)break
           this.values[index]=this.values[swap]
           this.values[swap]=element
       }
    }
}
class Node{
    constructor(val,priority){
        this.val=val
        this.priority=priority
    }
}
var obj=new PriorityQueue()
obj.enqueue("Akandison",7)
obj.enqueue("Biwa",6)
obj.enqueue("Makoah",2)
obj.enqueue("AT Rich",5)