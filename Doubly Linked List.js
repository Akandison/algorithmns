class Node{
    constructor(val){
        this.val=val
        this.next=null
        this.prev=null
    }
}
class DoublyLinkedList{
    constructor(){
        this.head=null
        this.tail=null
        this.length=0
    }
    push(val){
        var newNode=new Node(val)
        var current=this.head
        if(!this.head){
            this.head=newNode
            this.tail=newNode
            
        }
        else{
        this.tail.next=newNode
        newNode.prev=this.tail
        this.tail=newNode
        }
        this.length++
        return this
    }
    pop(){
        var poppedNode=this.tail
      if(!this.head) return undefined
      if(this.head==1){
          this.head=null
          this.tail=null
      }
      else{
          this.tail=poppedNode.prev
          this.tail.next=null
          poppedNode.prev=null 
      }
      this.length--
      return this
    }
    remHead(){
        if(!this.head)return false
        if(this.length==1){
            this.head-null
            this.tail=null
        }
        var current=this.head
        var newHead=current.next
        newHead.prev=null
        current.next=null
        this.head=newHead
        this.length--
        return current
    }
    addHead(val){
        var newNode=new Node(val)
        if(this.length==0){
            this.head=newNode
            this.tail=newNode
        }
        
        this.head.prev=newNode
        newNode.next=this.head
        this.head=newNode
        this.length++
        return this
       
    }
    get(index){
        var counter
        if(index<0 || index>this.length) return false
       if(index==0) return this.head
        if(index==this.length-1) return this.tail
        var current

        if(index<this.length/2){
            
            counter=0
            current=this.head
        while(index!=counter){
            current=current.next
            counter++
        }
        }
        else if(index>=this.length/2){
          
            current=this.tail
            counter=this.length-1
            while(index!=counter){
                current=current.prev
                counter--
            }
        }
        return current

    }
    set(index,value){
        if(index<0 || index>this.length)return false
        var current
        var counter
        if(index<this.length/2){
            current=this.head
            counter=0
            while(index!=counter){
                current=current.next
                counter++
            }
        }
        if(index>=this.length/2){
            current=this.tail
            counter=this.length-1
            while(index!=counter){
                current=current.prev
                counter--
            }
            
        }
        current.val=value
        return this

    }
    print(){
        var arr=[]
        var current=this.head
        while(current){
            arr.push(current.val)
            current=current.next
        }
        return arr
    }
    insert(index,val){
        if(index<0)return false
        else if(index==0)return this.addHead(val)
        else if(index==this.length)return this.push(val)
        else{
            var newNode=new Node(val)
            var prevNode=this.get(index-1)
            var aheadNode=this.get(index)
            prevNode.next=newNode
            newNode.prev=prevNode
            newNode.next=aheadNode
            aheadNode.prev=newNode
            this.length++
            return true
        }
        
    }
    removeNode(index){
        if(index<0 || index>=this.length)return false
        else if(index==0){
            var newHead=this.get(1)
            newHead.prev=null
            this.head.next=null
            this.head=newHead
            
        }
        else if(index==this.length-1){
            var preTail=this.get(index-1)
            preTail.next=null
            this.tail.prev=null
            
        }
        else{
              var remNode=this.get(index)
              var preNode=remNode.prev
              var aheadNode=remNode.next
              preNode.next=aheadNode
              aheadNode.prev=preNode
              remNode.next=null
              remNode.prev=null

        }
      
        this.length--
        return this
    }
}
var obj=new DoublyLinkedList()
obj.push("67")
obj.push("Hello")
obj.push("Olumbe")
obj.push("How")
obj.push("hunter")
obj.push("Adwoa")
obj.print()
//obj.insert(4,"Gbona")
//obj.print()
obj.removeNode(2)
obj.print()