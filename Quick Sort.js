function GetP(arr,start=0,end=arr.length-1){
    startIndex=start
    pivot=arr[start]
    for(i=start+1;i<arr.length;i++){
        if(pivot>arr[i]){
            startIndex++
    swap(arr,startIndex,i)
    
    }
    }
    swap(arr,start,startIndex)
   
    return startIndex

}
function swap(arr,i,j){
    temp=arr[i]
    arr[i]=arr[j]
    arr[j]=temp
}

function quickSort(arr,left=0,right=arr.length){
    if(left<right){
        let  pivotIndex=GetP(arr,left,right)
          quickSort(arr,left,pivotIndex-1)
          quickSort(arr,pivotIndex+1,right)
    }
    
     return arr  
}
 quickSort([23,72,15,17,20])
