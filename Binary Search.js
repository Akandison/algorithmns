function binarysearch(sortedArray,searchTerm){
    var left=0;
    var right=sortedArray.length-1;
    var index=-1;
    while(left<right){
      var  middle=Math.floor((left+right)/2);
        if(searchTerm==sortedArray[middle]){
            return index=middle;
        }
        if(searchTerm>middle){
            left=middle+1;
            middle=Math.floor((left+right)/2);
        }
        if(searchTerm<middle){
            right=middle-1;
            middle=Math.floor((left+right)/2);
        }
    }
    return index;
}
console.log(binarysearch([1,2,3,4,5,10,13,15,16,18,19,24,26,28],19))
