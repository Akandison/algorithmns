class Heaps{
    constructor(){
        this.values=[]
    }
        insert(val){
            this.values.push(val)
            this.bubble()
    }
    bubble(){
        let index=this.values.length-1
        const element=this.values[index]
        while(index>0){
        let parentIndex=Math.floor((index-1)/2)
        let parent=this.values[parentIndex]
        if(element<=parent)break
        this.values[parentIndex]=element
        this.values[index]=parent
        index=parentIndex
        } 
    }
    removeHead(){
        let max=this.values[0]
        let end=this.values.pop()
        if(this.values.length>0){
        this.values[0]=end
        this.sinkDown()
    }
        return max
    }
    sinkDown(){
       let index=0
       const length=this.values.length
       const element=this.values[0]

       while(true){
          let leftIndex=2*index+1
           let rightIndex=2*index+2
           let swap=null
           let rightChild,leftChild
           if(leftIndex<length){
                leftChild=this.values[leftIndex]
               if(element<leftChild){
                   swap=leftIndex
               }
           }
           if(rightIndex<length){
                rightChild=this.values[rightIndex]
               if(swap==null && element<rightChild || swap!=null && rightChild>leftChild){
                   swap=rightIndex
               }
           }
           if(swap==null)break
           this.values[index]=this.values[swap]
           this.values[swap]=element
       }
    }
     g 
}
var obj=new Heaps()
obj.insert(41)
obj.insert(39)
obj.insert(33)
obj.insert(18)
obj.insert(27)
obj.insert(12)

obj.insert(55)