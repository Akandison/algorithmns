class Node{
    constructor(val){
        this.val =val;
        this.next=null;
    }
}
class SinglyLinkedList{
    constructor(){
        this.length=0
        this.head=null
        this.tail=null
    }
    push(val){
       var newNode=new Node(val)
        if(!this.head){
            this.head=newNode
            this.tail=this.head
        }
        this.tail.next=newNode
        this.tail=newNode
        this.length++
        return this
    }
    traverse(){
        var current =this.head
        while(current){
            console.log(current.val)
            current=current.next
        }
    }

    pop(){
        if(!this.head) return undefined
        var current=this.head
        var newTail=current
        while(current.next){
             newTail=current
            current=current.next
           
        }
        this.tail=newTail
        this.tail.next=null
        this.length--
        if(this.length==0){
            this.head=null
            this.tail=null
        }
        return current
    }
    remHead(){
        if(!this.head)return undefined
        var newHead=this.head
        this.head=newHead.next
        this.length--
        if(this.length==0){
            this.tail=null
        }
        return newHead
    }
    addHead(val){
          var addedNode=new Node(val)
        if(!this.head){
            this.head=addedNode
            this.tail=addedNode
        }
        else{
            addedNode.next=this.head
        this.head=addedNode
        }
        
        this.length++
        return this
    }
    getNode(index){
        var counter=0
        if(index<0 || index>=this.length) return null
        var current=this.head
        while(current){
            if(counter==index) return current
             //else if(!current.next) return null
            current=current.next
            counter++
        }
    }
    setNode(index,newValue){
        var retrievedNode=this.getNode(index)
        if(retrievedNode) {retrievedNode.val=newValue
         return true}
       return false
            }

     Insert(index,value){
         var newNode=new Node(value)
         if(index<0 || index>this.length)return false
         else if(index==this.length){
             this.push(value)
         }
         else if(index==0){
             this.addHead(value)
         }

         var counter =0
         var current=this.head
         var currentBack=current
         while(counter !=index){
             currentBack=current
             current=current.next

             counter++
         }
         currentBack.next=newNode
         newNode.next=current
         this.length++
         return true
     }
     removeNode(index){
         if(index<1 || index>this.length)return false
         else if(index==this.length-1){
             this.pop()
         }
         else if (index==0) this.remHead()
         var prevNode=this.getNode(index-1)
         var current=this.getNode(index+1)
         if(prevNode){
             prevNode.next=current
             this.length--
             return true
         }

     }
     print(){
         var arr=[]
         var current=this.head
         while(current){
             arr.push(current.val)
             current=current.next
         }
         console.log(arr)
     }
     reverse(){
         var current=this.head
         this.head=this.tail
         this.tail=current
         var next
         var prev=null
         for(var i=0;i<this.length;i++){
             next=current.next
             current.next=prev
             prev=current
             current=next
         }
         return this
     }
}
var obj=new SinglyLinkedList()
obj.push("Hi")
obj.push("Helkim")
obj.push("Multim")
obj.push("JHg")
obj.push("Gyakwata")
//obj.traverse()
// obj.remHead()
// obj.traverse()
//obj.addHead(78)
//obj.traverse()
//obj.getNode(5)
//obj.length
//obj.Insert(2,"Ogolo")
//obj.traverse()
//obj.removeNode(3)
obj.print()
obj.reverse()
obj.print()