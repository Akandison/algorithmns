class HashTables{
    constructor(keySize=4){
        this.keyMap=new Array(keySize)
    }
 _Hash(key){
    var somePrime=31
    var total=0
    for(var i=0;i<Math.min(key.length,100);i++){
       let char=key[i]
       let values=char.charCodeAt(0)-96
        total = (total * somePrime + values)%this.keyMap.length
    }
    return total
}
set(key,value){
    var index=this._Hash(key)
    if(!this.keyMap[index]){
        this.keyMap[index]=[]
    }
    this.keyMap[index].push([key,value])
}
get(key){
    var  index=this._Hash(key)
    if(this.keyMap[index]){
     for(let i=0;i<this.keyMap[index].length;i++){
         if(key==this.keyMap[index][i][0]){
             return this.keyMap[index][i][1]
         }
     }
    }
    return console.log("value for " + key + " not found")
}
values(){
    let valArr=[]
    for(let i=0;i<this.keyMap.length;i++){
        if(this.keyMap[i]){
            for(let j=0;j<this.keyMap[i].length;j++){
                if(!valArr.includes(this.keyMap[i][j][1])){
                      valArr.push(this.keyMap[i][j][1])
                }
              
            }
        }
    }
    return valArr
}
keys(){
    let keyArr=[]
    for(let i=0;i<this.keyMap.length;i++){
        if(this.keyMap[i]){
            for(let j=0;j<this.keyMap[i].length;j++){
                if(!keyArr.includes(this.keyMap[i][j][0])){
                      keyArr.push(this.keyMap[i][j][0])
                }
              
            }
        }
    }
    return keyArr
}
}
var obj=new HashTables()
obj.set("joe","Hits")
obj.set("Ashanti","Kumasi")
obj.set("Ahafo","Goaso")
obj.set("Ofoase","Kokoben")
obj.keys()