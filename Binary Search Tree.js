//node class
class Node{
    constructor(val){
        this.val=val
        this.right=null
        this.left=null
    }
}
//Binary search tree class
class BST{
    constructor(){
        this.root=null
    }
    //adding new node
    insert(value){
        var newNode=new Node(value)
        
         if(this.root==null) {
          this.root=newNode
         return this
         }
         else{
               var current=this.root
        while(true){
          
            if(value<current.val){
                if(current.left==null) {
                current.left=newNode
                return this}
                else {
                    current=current.left
                }
            }
            else if(value>current.val){
                if(current.right==null) {current.right=newNode
                return this}
                else{
                    current=current.right
                }
            }

        }
       
         }
    }
    search(value){
        if(this.root===null) return null
        else{
            var current=this.root
            var found=false
            while(current && !found){
            if(value>current.val){
                current=current.right
                
            }
            else if(value<current.val){
                current=current.left
                
            }
            else{ return current
            }
        }
        console.log("Not found")

    
        }  
    }
    BFS(){
        var queue=[]
        var visited=[];
          var node =this.root
        queue.push(node)
      
        while(queue.length){
            node=queue.shift()
            visited.push(node.val)
            if(node.left)queue.push(node.left)
            if(node.right)queue.push(node.right)
        }
        return visited
    }
    PreOrder(){
        var visited=[]
        var current=this.root
       function traverse(node){
            visited.push(node.val)
            if(node.left)traverse(node.left)
            if(node.right)traverse(node.right)
        }
        traverse(current)
        return visited
    }
    PostOrder(){
        var current=this.root
        var visited=[]
        function traverse(node){
            if(node.left)traverse(node.left)
            if(node.right)traverse(node.right)
            visited.push(node.val)
        }
        traverse(current)
        return visited
    }
    InOrder(){
        var current=this.root
        var visited=[]
        function traverse(node){
            if(node.left)traverse(node.left)
            visited.push(node.val)
            if(node.right)traverse(node.right)
        }
        traverse(current)
        return visited
    }
   
}
var obj=new BST()
    obj.insert(10)
    obj.insert(6)
    obj.insert(15)
    obj.insert(3)
    obj.insert(8)
    obj.insert(20)
    obj.InOrder()
