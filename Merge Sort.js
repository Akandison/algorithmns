function mergeSort(arr1,arr2){
    var i=0
    var j=0
    let results=[]
    while(i<arr1.length && j<arr2.length ){
        if(arr1[i] < arr2[j]){
            results.push(arr1[i])
            i++
        }
        if(arr2[j]<arr1[i]){
            results.push(arr2[j])
            j++
        }
    }
    while(i<arr1.length){
        results.push(arr1[i])
        i++
    }
    while(j<arr2.length){
        results.push(arr2[j])
        j++
    }
    return results

}
function sort(arr){
    if(arr.length<=1) return arr
    mid= Math.floor((arr.length)/2)
    left=sort(arr.slice(0,mid))
    right=sort(arr.slice(mid))
    return merge(left,right)
}
console.log(sort([23,5,87,90,75,35]))
